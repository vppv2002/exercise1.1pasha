package com.company;

import java.util.Scanner;

public class Main {
    private final static int BIG_INT = Integer.MAX_VALUE;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество денег: ");
        int amountOfMoney = Integer.parseInt(scan.nextLine());
        System.out.println("Введите пункт обмена денег (ПриватБанк, Обменник, Черный рынок): ");
        String nameOfExchangers = scan.nextLine();

        Exchange[] exchangers = new Exchange[3];
        exchangers[0] = new Exchange("ПриватБанк", 27.4, 150000, BIG_INT);
        exchangers[1] = new Exchange("Обменник", 27.25, BIG_INT, 20000);
        exchangers[2] = new Exchange("Черный рынок", 27.1, BIG_INT, BIG_INT);

        for (Exchange exchange : exchangers) {
            System.out.println(exchange.bankExchange(amountOfMoney, nameOfExchangers));
        }
    }
}
