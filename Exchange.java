package com.company;

public class Exchange {
    private int limitUAH;
    private int limitUSD;
    private String name;
    private double course;

    public Exchange(String name, double course, int limitUAH, int limitUSD) {
        this.name = name;
        this.course = course;
        this.limitUAH = limitUAH;
        this.limitUSD = limitUSD;
    }


    public String bankExchange(double amount, String nameOfExchangers) {
        if (name.equalsIgnoreCase(nameOfExchangers)) {
            if (amount < limitUAH) {
                amount = amount / course;
                if (amount < limitUSD) {
                    return String.format("%s, Cумма в долларах: %.2f", name, amount);
                } else {
                    return String.format("%s: Превышен лимит", name);
                }
            } else {
                return String.format("%s: Превышен лимит", name);
            }

        }
        return String.format("");
    }
}